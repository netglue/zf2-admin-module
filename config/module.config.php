<?php
return array(
	
	'navigation' => array(
		// 'ngadmin' key corresponds to the factory method getName() in AdminNavigationFactory
		'ngadmin' => array(
			'home' => array(
				'label' => ' Dashboard',
				'class' => 'icon icon-dashboard',
				'route' => 'ngadmin',
			),
		),
	),
	
	'service_manager' => array(
		'factories' => array(
			// Use 'admin-nav' form the navigation() view helper. ie. $this->navigation('admin-nav');
			'admin-nav' => 'NgAdmin\Navigation\Service\AdminNavigationFactory',
		),
	),
	
	'router' => array(
		'routes' => array(
			'ngadmin' => array(
				'type' => 'literal',
				'options' => array(
					'route' => '/admin',
					'defaults' => array(
						'controller' => 'NgAdmin\Controller\IndexController',
						'action' => 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(),
			),
		),
	),
	
	'view_manager' => array(
		// Key the admin layout so it can be easily overridden
		'template_map' => array(
			'ng_admin_layout' => __DIR__ . '/../view/layout/admin.phtml',
			'ng-admin/index/index'  => __DIR__ . '/../view/ng-admin/index/index.phtml',
		),
	),
	
	'controllers' => array(
		'invokables' => array(
			'NgAdmin\Controller\IndexController' => 'NgAdmin\Controller\IndexController',
		),
	),
	
);
