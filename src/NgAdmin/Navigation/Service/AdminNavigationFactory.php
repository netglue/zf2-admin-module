<?php

namespace NgAdmin\Navigation\Service;

use Zend\Navigation\Service\DefaultNavigationFactory;

class AdminNavigationFactory extends DefaultNavigationFactory {
	
	public function getName() {
		return 'ngadmin';
	}
	
}