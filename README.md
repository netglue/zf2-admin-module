#Generic Admin Module

Based on the [ZfcAdmin module](https://github.com/ZF-Commons/ZfcAdmin).

If you want something that will develop in a more predicatable way - you're probably best off with that. This module is mostly for my own needs and won't be too concerned with BC breaks though I'll try to tag it semantically as it gets developed so there's not too much in the way of surprises.

##Install
Standard composer install. Composer name is `netglue/zf2-admin` and module name is `NgAdmin`

Once installed, the default view script has some docs in it and will be located at `/admin` by default.

